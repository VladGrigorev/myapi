<?php
class User
{
    public function login($login, $password)
    {
        $login = htmlspecialchars($login);

        $domain = "https://api.sandbox.nrg-tk.ru/v2/login?user=$login&password=$password";
        $ch = curl_init($domain);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        $data = json_decode($data, true);
        curl_close($ch);
        return $data;
    }
}
