<?php
class Client
{

    public function getClients($accountId, $token)
    {

        $domain = "https://api.sandbox.nrg-tk.ru/v2/$accountId/clients?token=$token";
        $ch = curl_init($domain);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        $data = json_decode($data, true);
        curl_close($ch);
        return $data;
    }

    public function createClient($accountId, $token, $dataArray)
    {
        $data_string = json_encode($dataArray);
        $domain = "https://api.sandbox.nrg-tk.ru/v2/$accountId/clients?token=$token";
        $ch = curl_init($domain);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_ENCODING, "utf-8");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        $data = json_decode($data, true);
        if(isset($data['code'])){
            if(strcmp($data['code'],"InternalServerError")===0){
                return -1;
            }
        }
        curl_close($ch);
        return $data;
    }

    public function deleteClient($accountId, $token, $clientId){
        $domain = "https://api.sandbox.nrg-tk.ru/v2/$accountId/clients/$clientId?token=$token";
        $ch = curl_init($domain);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        $data = json_decode($data, true);
        curl_close($ch);
        if(isset($data['code'])){
            if(strcmp($data['code'],"")===0){
                return 1;
            }
            elseif(strcmp($data['code'],"NotFound")===0){
                return -1;
            }
        }


    }
    public function getClient($accountId, $token, $clientId){
        $domain = "https://api.sandbox.nrg-tk.ru/v2/$accountId/clients/$clientId?token=$token";
        $ch = curl_init($domain);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        $data = json_decode($data, true);
        curl_close($ch);
        if(isset($data['id'])){
            return $data;
        }
        elseif(isset($data['code'])){
            if(strcmp($data['code'],"NotFound")===0){
                return -1;
            }
        }
    }

    public function updateClient($accountId, $token, $clientId, $dataArray){
        $data_string = json_encode($dataArray);
        $domain = "https://api.sandbox.nrg-tk.ru/v2/$accountId/clients/$clientId?token=$token";
        $ch = curl_init($domain);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_ENCODING, "utf-8");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        $data = json_decode($data, true);
        curl_close($ch);

        if(isset($data['id'])){
            return $data;
        }
        elseif(isset($data['code'])){
            if(strcmp($data['code'],"NotFound")===0){
                return -1;
            }
        }
        return $data;
    }

}