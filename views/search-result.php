<?php
    global $result;
    //var_dump($GLOBALS['result']);
?>
<div class="container">
    <div class="search_block">
        <div class="head">
            <?= $result['title']; ?>
        </div>
        <?php foreach ($result as $key => $value): ?>
            <div class="info">
                <div class="left">
                    <?= $key; ?>
                </div>
                <div class="right">
                    <?= $value; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
