<div class="container">
    <div class="header">
        Клиенты
    </div>
    <?php foreach ($data as $item): ?>
    <div class="block">
        <div class="head">
            <?= $item['title']; ?>
        </div>
        <?php foreach ($item as $key => $value): ?>
            <div class="info">
                <div class="left">
                    <?= $key; ?>
                </div>
                <div class="right">
                    <?= $value; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php endforeach; ?>
</div>
