<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <title>ClientApi</title>
</head>
<body>

<div class="navbar">
    <?php if($_SESSION['isLogin']): ?>
    <div class="inside">
    <div class="navbar-button">
        <a href="?r=site/index">
            Список клиентов
        </a>
    </div>
    <div class="navbar-button">
        <a href="?r=site/createClient">
            Создать нового клиента
        </a>
    </div>
    <div class="navbar-button">
        <a href="?r=site/deleteClient">
            Удалить клиента
        </a>
    </div>
    <div class="navbar-button">
        <a href="?r=site/searchClient">
            Поиск клиента
        </a>
    </div>
    <div class="navbar-button">
        <a href="?r=site/updateClient">
            Обновить информацию или создать нового клиента
        </a>
    </div>
    </div>
    <?php endif; ?>
</div>
<?= $content; ?>
<div class="outer-block">

</div>
<div class="footer">

</div>

</body>
</html>

