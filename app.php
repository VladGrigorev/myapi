<?php
include "autoloader.php";

class App
{
    protected static $instance;
    private $controller;
    private $action;

    public static function getInstance()
    {
        if (null === static::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function route()
    {
        $actual_link = "$_SERVER[REQUEST_URI]";

        $match = [];
        if (strcmp($actual_link, '/') !== 0) {
            preg_match('/\/?r=(.*)/', $actual_link, $match);
            $route = explode("/", $match[1]);
            $this->controller = trim($route[0]);
            $this->action = trim($route[1]);
        }
    }

    public function getPage()
    {
        $controller = ucfirst($this->controller);
        if (empty($controller)) {
            $controller = "Site";
        }
        $class = $controller . "Controller";
        if(class_exists($class)) {
            $controller = new $class();
        }
        else{
            echo "404 error";
            return;
        }
        $action = $this->action;
        if (empty($action)) {
            $action = "index";
        }
        if(method_exists($class,$action)){
            $controller->$action();
        }
        else{
            echo "404 error";
            return;
        }

    }
}


