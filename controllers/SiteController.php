<?php
include "models/User.php";
include "models/Client.php";

class SiteController extends Controller
{


    function __construct()
    {
        if (isset($_SESSION['isLogin']) && !$_SESSION['isLogin']) {
            $_SESSION['isLogin'] = false;
        }
    }




    public function login()
    {
        $user = new User;
        if (isset($_POST['user']) && isset($_POST['password'])) {
            $data = $user->login($_POST['user'], $_POST['password']);
            if (isset($data['token'])) {
                $_SESSION['token'] = $data['token'];
                $_SESSION['accountId'] = $data['accountId'];
                $_SESSION['isLogin'] = true;
            }
            $this->redirect('index');
        }
        $this->render('login');

    }

    public function index()
    {
        if ($_SESSION['isLogin']) {
            $client = new Client;
            $data = $client->getClients($_SESSION['accountId'], $_SESSION['token']);
            $this->render('index', ['data' => $data]);

        } else {
            $this->redirect('login');
        }
    }

    public function createClient()
    {
        $client = new Client;
        if (!empty($_POST)) {
            $dataArray = [];
            $_POST['idCity'] = (int)$_POST['idCity'];
            foreach ($_POST as $key => $value) {
                $dataArray[$key] = $value;
            }
            $result = $client->createClient($_SESSION['accountId'], $_SESSION['token'], $dataArray);
            if ($result == -1) {
                global $alert;
                $alert = 'Wrong Country Id!';
                $this->render('create');
            }
            $this->redirect('index');
        }
        $this->render('create');
    }

    public function deleteClient()
    {
        $client = new Client;
        if (!empty($_POST)) {
            $result = $client->deleteClient($_SESSION['accountId'], $_SESSION['token'], $_POST['clientId']);
            if ($result == 1) {
                $this->redirect('index');
            } elseif ($result == -1) {
                global $alert;
                $alert = 'Client not found!';
                $this->render('delete');
            }

        }
        $this->render('delete');
    }

    public function searchClient()
    {
        $client = new Client;
        if (!empty($_POST)) {
            $result = $client->getClient($_SESSION['accountId'], $_SESSION['token'], $_POST['clientId']);
            if (is_array($result) && isset($result['id'])) {
                $this->render('search-result',['result'=>$result]);
            } elseif ($result == -1) {
                global $alert;
                $alert = 'Client not found!';
                $this->render('search');
            }

        }
        $this->render('search');
    }
    public function updateClient()
    {
        $client = new Client;
        if (!empty($_POST)) {
            $dataArray = [];
            $_POST['idCity'] = (int)$_POST['idCity'];
            foreach ($_POST as $key => $value) {
                $dataArray[$key] = $value;
            }
            array_shift($dataArray);
            $result = $client->updateClient($_SESSION['accountId'], $_SESSION['token'], $_POST['clientId'],$dataArray);
            if (is_array($result) && isset($result['id'])) {
                $this->redirect('index');
            } elseif ($result == -1) {
                global $alert;
                $alert = 'Client not found!';
                $this->render('update');
            }

        }
        $this->render('update');
    }
}