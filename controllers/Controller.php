<?php
class Controller {
    protected function getControllerName(){
        $class_name=get_called_class();
        $match=[];
        preg_match('/(.*)Controller/',$class_name,$match);
        $name = strtolower($match[1]);
        return $name;
    }
    protected function redirect($action, $vars = null)
    {
        //$controller = $this->getControllerName();
        $this->$action();
    }
    private function getContent($view,$vars)
    {
        ob_start();
        if (!empty($vars)) {
            foreach ($vars as $key => $value) {
                $$key = $value;
            }
        }
        include "views/$view";
        return ob_get_clean();
    }
    protected function render($view, $vars = null)
    {
        $view = $view . '.php';
        $content = $this->getContent($view,$vars);
        include "views/layout.php";
        exit;
    }

}